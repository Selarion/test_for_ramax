class MyError(Exception):
    def __init__(self):
        super().__init__('Error text')


class Parent:
    f_flag = None
    s_flag = None

    def __init__(self, i=3):
        self.i = i

        # Игра в качели со статичными полями, чтобы у кажого нечетного было 1-0, а у каждого четного 0-1.
        if not Parent.f_flag:
            Parent.f_flag, Parent.s_flag = 1, 0
        elif not Parent.s_flag:
            Parent.f_flag, Parent.s_flag = 0, 1
        self._isFirst, self.isSecond = Parent.f_flag, Parent.s_flag

    def __setattr__(self, key, value):
        # Проверка на диапозан у поля isSecond. Четких укзаание нет, так что проверял только его.
        if key == 'isSecond' and (value > 1 or value < 0):
            raise AttributeError()
        super().__setattr__(key, value)

    def isFirst(self):
        return self._isFirst


class A(Parent):
    def __init__(self):
        super().__init__()

    def fnc(self, num):
        if num >= 3:
            raise MyError()
        return num * 2 * self.i