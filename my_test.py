# -*- coding: utf-8 -*-
"""Test Module."""

from ma import *
from mb import B

First = A
Second = B


def test():
    a = A()
    b = B(5)

    assert(a.i == 3)
    assert(a.fnc(2) == 2 * 2 * 3)
    assert(b.fnc(10, 4) == 10 * 4 * 5)

    # Не понял, если честно, почему обращение к isFirst () - callable, а к isSecond - просто как к полю. Но окей,
    # прокси-метод-сеттер написать не сложно.
    assert(a.isFirst() == 1)
    assert(a.isSecond == 0)
    assert(b.isFirst() == 0)
    assert(b.isSecond == 1)

    # Долго думал и либо я не понял, на что тут намек. Либо предполагалась более длинное наследование, чем написал я.
    # Тогда isinstance, который поддерживает проверку по доченим классам (о отличии от type) выглядел бы более понятно,
    # Либо действительно нужен был какой-то непонятный костыл line 7, 8
    # Но более длинное наследование... зачем?
    assert(isinstance(a, First))
    assert(isinstance(b, Second))
    assert(isinstance(a, Parent))
    assert(isinstance(b, Parent))

    try:
        # Предположил, что isSecond и isFirst - должны булево скакать 0-1. Но какая проверка подразумевалась -
        # не очевидно.
        a.isSecond = 2
    except AttributeError:
        pass
    else:
        assert(0)

    try:
        # Еще одна неочевидная проверка. Допустим, имелос ввиду, что тут надо вызвать кастомное исключение.
        a.fnc(7)
    except MyError as v:
        if str(v) != "Error text":
            assert(0)
    else:
        assert(0)


if __name__ == "__main__":
    test()
    print("done")
