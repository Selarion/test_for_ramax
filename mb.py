from ma import Parent


class B(Parent):
    def __init__(self, i):
        super().__init__(i)

    def fnc(self, first_num, second_num):
        """Теоритически конечно можно объединить эту реализацию с одноименной у класса А с помощью именнованных
        аргументов и вынести это все в родительский класс. Но это скорее кажется вредным, чем полезным."""
        return first_num * second_num * self.i